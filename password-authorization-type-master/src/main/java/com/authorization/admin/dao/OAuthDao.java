package com.authorization.admin.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;

import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.oauthEntity.UserEntity;
import com.authorization.admin.repository.UserRepository;

@Repository
public class OAuthDao {
	@Autowired
	private UserRepository userRepository;

	public UserEntity getUserDetails(String username) {
		Optional<User> user = userRepository.findByUsernameAndEnabled(username, true);
		Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
		List<UserEntity> list = new ArrayList<UserEntity>();
		if (user.isPresent()) {
			UserEntity entity = new UserEntity();
			entity.setUsername(user.get().getUsername());
			entity.setPassword(user.get().getPassword());
			user.get().getRoles().stream().forEach(authority->{
				grantedAuthoritiesList.add(new SimpleGrantedAuthority("ROLE_"+authority.getName()));
			});
			System.err.println(grantedAuthoritiesList.toString());
			entity.setGrantedAuthoritiesList(grantedAuthoritiesList);
			list.add(entity);
			if (list.size() > 0) {
				return list.get(0);
			}
		}
		return null;
	}
}
