package com.authorization.admin.dto;

import java.util.List;

public class GoogleSsoResponseDto {
	
	private List<GoogleAuthority> authorities;


	public List<GoogleAuthority> getAuthorities() {
		return authorities;
	}


	public void setAuthorities(List<GoogleAuthority> authorities) {
		this.authorities = authorities;
	}


	@Override
	public String toString() {
		return "GoogleSsoResponseDto [attributes=" + authorities + "]";
	}
	
	
	
	

}
