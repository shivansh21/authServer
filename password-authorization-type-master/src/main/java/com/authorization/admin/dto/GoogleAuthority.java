package com.authorization.admin.dto;

public class GoogleAuthority {
	private String authority;
	private Object attributes;
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}

	
	public Object getAttributes() {
		return attributes;
	}
	public void setAttributes(Object attributes) {
		this.attributes = attributes;
	}
	public GoogleAuthority() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "GoogleAuthority [authority=" + authority + ", attributes=" + attributes + "]";
	}
	
	
	
	
	
	
	
	

}
