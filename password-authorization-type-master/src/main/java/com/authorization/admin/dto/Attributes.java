package com.authorization.admin.dto;

public class Attributes {
	
	private String email;
	private String family_name;
	private String given_name;
	private String name;
	private String picture;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFamily_name() {
		return family_name;
	}
	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}
	public String getGiven_name() {
		return given_name;
	}
	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "Attributes [email=" + email + ", family_name=" + family_name + ", given_name=" + given_name
				+ ", name=" + name + ", picture=" + picture + "]";
	}
	
	

}
