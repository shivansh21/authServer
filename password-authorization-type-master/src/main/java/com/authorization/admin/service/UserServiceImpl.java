package com.authorization.admin.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.repository.UserRepository;
import com.authorization.admin.serviceInterface.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class.getName());

	@Override
	public User registerUser(User user) throws Exception {
		User createdUser = null;
		if (!userRepository.existsByUsername(user.getUsername())) {
			logger.info("Username does not exists");
			BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
			if(user.getPassword()!=null)
			{
			try {
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			}
			  catch (Exception e) {
				logger.error("Exception cought "+e.getMessage());
				throw e;
			}
			}
			logger.info("Adding the user "+user.getUsername());
			createdUser  =  userRepository.save(user);
		} else {
			logger.error("Username already exists");
			throw new Exception("Username Exists!");
		     
		}
		return createdUser;
	}

}
