package com.authorization.admin.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.authorization.admin.oauthEntity.Role;
import com.authorization.admin.repository.RoleRepository;
import com.authorization.admin.serviceInterface.RoleService;

@Service
public class RoleServiceImpl implements RoleService {
	private static final Logger logger = LogManager.getLogger(RoleServiceImpl.class.getName());

	@Autowired
	private RoleRepository  roleRepository;

	@Override
	public Role createRole(Role role) throws Exception {
		Role createdRole = null;
		try {
		createdRole = roleRepository.save(role);	
		logger.info("role created...");
		}
		catch (Exception e) {
			logger.error("Excepion occured in Role service during ceation of "+role.toString());
			throw new Exception("Exception caught during role creation");
		}
		return createdRole;
	}

}
