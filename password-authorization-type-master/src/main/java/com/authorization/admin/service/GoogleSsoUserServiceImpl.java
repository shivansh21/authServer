package com.authorization.admin.service;

import java.util.HashSet;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.authorization.admin.dto.Attributes;
import com.authorization.admin.dto.GoogleSsoResponseDto;
import com.authorization.admin.oauthEntity.Role;
import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.repository.RoleRepository;
import com.authorization.admin.repository.UserRepository;
import com.authorization.admin.serviceInterface.GoogleSsoUserService;
@Service
public class GoogleSsoUserServiceImpl implements GoogleSsoUserService {
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public Boolean userExists(User user) {
		// TODO Auto-generated method stub
		return userRepository.existsByEmail(user.getEmail());
	}

	@Override
	public User setSsoUserDetails(GoogleSsoResponseDto response) {

		Set<Role> roles = new HashSet<Role>();
		roles.add(roleRepository.findByName("USER"));
		System.err.println(response.getAuthorities().toString());
		Attributes attributes = mapper.map(response.getAuthorities().get(0).getAttributes(), Attributes.class) ;
		User user = new User(attributes.getEmail(), attributes.getEmail(), null, null, "google", true, roles);
		return user;
	}

}
