package com.authorization.admin.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.authorization.admin.oauthEntity.Role;
import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.response.SuccessResponse;
import com.authorization.admin.service.UserServiceImpl;
import com.authorization.admin.serviceInterface.RoleService;
import com.authorization.admin.serviceInterface.UserService;

@RestController
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());

	@PostMapping(value = "/registerUser")
	private ResponseEntity<Object> registerUser(@RequestBody User user) {
		logger.info("Request Entered into controler");
		User createdUser = null;
		try {
			createdUser = userService.registerUser(user);
			logger.info("User " + user.getUsername() + " created successfully");
		} catch (Exception e) {
			logger.error("Exception caught in User " + user.getUsername() + " creation");
			logger.error("Exiting after exception");
			return new ResponseEntity<Object>(new SuccessResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, "exception occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(new SuccessResponse(HttpStatus.OK, createdUser, "user added successfuly"), HttpStatus.OK);

	}

	@PostMapping(value = "/createRole")
	private ResponseEntity<Object> createRole(@RequestBody Role role) {
		Role createdRole = null;
		logger.info("Request Entered into controler");
		try {
			createdRole = roleService.createRole(role);
			logger.info("Role " + role.getName() + " created successfully");
		} catch (Exception e) {
			logger.error("Exception caught in Role " + role.getName() + " creation");
			logger.error("Exiting after exception");
			return new ResponseEntity<Object>(new SuccessResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, "exception occured"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(new SuccessResponse(HttpStatus.OK, createdRole, "Role added successfuly"), HttpStatus.OK);

	}

}
