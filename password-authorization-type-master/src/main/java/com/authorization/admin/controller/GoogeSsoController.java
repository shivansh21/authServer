package com.authorization.admin.controller;

import java.security.Principal;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.authorization.admin.AuthorizationServerApplication;
import com.authorization.admin.dto.GoogleSsoResponseDto;
import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.repository.UserRepository;
import com.authorization.admin.serviceInterface.GoogleSsoUserService;
import com.authorization.admin.serviceInterface.UserService;
import com.authorization.admin.tokenUtil.TokenUtil;

@RestController
public class GoogeSsoController {
	
	private final static Logger logger = Logger.getLogger(GoogeSsoController.class);
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TokenUtil tokenUtil;
	@Autowired
	private GoogleSsoUserService googleSsoUserService;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private UserService userService;
	
	

	@GetMapping(value = "/google/auth")
	public OAuth2AccessToken welcome(Principal principal) throws Exception {
		OAuth2AccessToken accessToken = null;
		try {
			logger.info("Request arrived for google login"+ principal.getName());
			GoogleSsoResponseDto ssoResponse = modelMapper.map(principal, GoogleSsoResponseDto.class);
			User user = googleSsoUserService.setSsoUserDetails(ssoResponse);
			boolean userExists = googleSsoUserService.userExists(user);
			if (userExists)
			{
				logger.info("User exists in our records");
				accessToken = tokenUtil.getAccessToken(user);
			}
			else {
				logger.info("User does not exists in our records");
				User createdUser = userService.registerUser(user);
				accessToken = tokenUtil.getAccessToken(createdUser);
			}

		} catch (Exception e) {
			logger.info("Exception occured during google login");
			e.printStackTrace();
			throw new Exception("Error occured");
			// TODO: handle exception
		}
		return accessToken;

	}



}
