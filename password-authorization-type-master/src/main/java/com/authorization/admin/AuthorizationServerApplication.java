package com.authorization.admin;

import java.security.Principal;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.authorization.admin.dto.GoogleSsoResponseDto;
import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.repository.UserRepository;
import com.authorization.admin.serviceInterface.GoogleSsoUserService;
import com.authorization.admin.serviceInterface.UserService;
import com.authorization.admin.tokenUtil.TokenUtil;

@SpringBootApplication
@EnableAuthorizationServer
@EnableResourceServer
@RestController
public class AuthorizationServerApplication {
	private final static Logger logger = Logger.getLogger(AuthorizationServerApplication.class);
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TokenUtil tokenUtil;
	@Autowired
	private GoogleSsoUserService googleSsoUserService;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private UserService userService;
	

	public static void main(String[] args) {
		BasicConfigurator.configure();
		logger.info("Info log message");
		SpringApplication.run(AuthorizationServerApplication.class, args);
	}
}
