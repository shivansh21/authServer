package com.authorization.admin.tokenUtil;

import java.security.KeyPair;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.stereotype.Component;

import com.authorization.admin.oauthConfig.AuthorizationServerConfig;
import com.authorization.admin.oauthEntity.Role;
import com.authorization.admin.oauthEntity.User;
import com.authorization.admin.repository.RoleRepository;

@Component
public class TokenUtil {

	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private AuthorizationServerConfig authorizationServerConfig;

	public OAuth2AccessToken getAccessToken(User user) {
		HashMap<String, String> authorizationParameters = new HashMap<String, String>();
		String clientId = "nerve24";
		authorizationParameters.put("scope", "read");
		authorizationParameters.put("username", user.getEmail());
		authorizationParameters.put("client_id", clientId);
		authorizationParameters.put("grant_type", "password");

		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
//		user.getRoles().forEach((role) -> {
//			Role rol = roleRepository.findByName(role.getName());
//			
//		});
		authorities.add(new SimpleGrantedAuthority("USER"));

		Set<String> responseType = new HashSet<String>();
		responseType.add("password");

		Set<String> scopes = new HashSet<String>();
		scopes.add("read");
		scopes.add("write");

		OAuth2Request authorizationRequest = new OAuth2Request(authorizationParameters, clientId, authorities, true,
				scopes, null, "", responseType, null);

		org.springframework.security.core.userdetails.User userPrincipal = new org.springframework.security.core.userdetails.User(
				user.getEmail(), "", authorities);
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userPrincipal,
				null, authorities);

		OAuth2Authentication authenticationRequest = new OAuth2Authentication(authorizationRequest,
				authenticationToken);
		authenticationRequest.setAuthenticated(true);
		OAuth2AccessToken accessToken = tokenServices().createAccessToken(authenticationRequest);

		return accessToken;
	}

//	@Bean
//	TokenEnhancerChain enhancerChain() {
//		TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
//		enhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
//		return enhancerChain;
//	}
//
//	@Bean
//	public JwtAccessTokenConverter accessTokenConverter() {
//		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource("keystore.jks"), "changeit".toCharArray())
//				.getKeyPair("mytest");
//		converter.setKeyPair(keyPair);
//
//		return converter;
//	}
//
//	@Bean
//	public TokenStore tokenStore() {
//		return new JwtTokenStore(accessTokenConverter());
//	}
//
//	@Bean
//	public JwtAccessTokenConverter tokenEnhancer() {
//		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource("keystore.jks"), "changeit".toCharArray())
//				.getKeyPair("mytest");
//		converter.setKeyPair(keyPair);
//
//		return converter;
//	}

	@Bean
	@Primary
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(authorizationServerConfig.tokenStore());
		defaultTokenServices.setSupportRefreshToken(true);
		defaultTokenServices.setTokenEnhancer(authorizationServerConfig.tokenEnhancer());
		return defaultTokenServices;
	}

}
