package com.authorization.admin.serviceInterface;

import org.springframework.stereotype.Service;

import com.authorization.admin.oauthEntity.User;

@Service
public interface UserService  {
	
	public User registerUser(User user) throws Exception;

}
