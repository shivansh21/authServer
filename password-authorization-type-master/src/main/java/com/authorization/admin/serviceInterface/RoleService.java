package com.authorization.admin.serviceInterface;

import org.springframework.stereotype.Service;

import com.authorization.admin.oauthEntity.Role;

@Service
public interface RoleService {
	
	public Role createRole(Role role) throws Exception;

}
