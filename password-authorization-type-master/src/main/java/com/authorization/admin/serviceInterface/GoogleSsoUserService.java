package com.authorization.admin.serviceInterface;

import org.springframework.stereotype.Service;

import com.authorization.admin.dto.GoogleSsoResponseDto;
import com.authorization.admin.oauthEntity.User;

@Service
public interface GoogleSsoUserService {
	
	public Boolean userExists(User user);
	public User setSsoUserDetails(GoogleSsoResponseDto object);

}
