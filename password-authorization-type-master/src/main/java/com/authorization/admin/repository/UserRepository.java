package com.authorization.admin.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.authorization.admin.oauthEntity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	public Optional<User> findByUsernameAndEnabled(String username,boolean flag);
	public Optional<User> findByEmailAndEnabled(String email,boolean flag);
	Boolean existsByEmail(String email);
	Boolean existsByUsername(String email);


	

}
