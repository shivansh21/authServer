package com.authorization.admin.oauthConfig;

import java.security.KeyPair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import com.authorization.admin.service.CustomDetailsService;

@Configuration
//@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	private CustomDetailsService authDao;

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Bean
	public JwtAccessTokenConverter tokenEnhancer() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource("keystore.jks"), "changeit".toCharArray())
				.getKeyPair("mytest");
		converter.setKeyPair(keyPair);

		return converter;
	}

	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(tokenEnhancer());
	}


	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}
	   

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory().withClient("nerve24").authorizedGrantTypes("password", "refresh_token")
				.secret(encoder().encode("pass123")).scopes("user_info", "read", "write")
				.accessTokenValiditySeconds(10 * 60).refreshTokenValiditySeconds(20 * 60)
				.redirectUris("http://localhost:8083/techgeeknext/login/oauth2/code/techgeeknextclient")
				.autoApprove(false).and().inMemory().withClient("nannyService")
				.authorizedGrantTypes("password", "refresh_token").secret(encoder().encode("nannyService@123"))
				.scopes("user_info", "read", "write").accessTokenValiditySeconds(10 * 60)
				.refreshTokenValiditySeconds(20 * 60)
				.redirectUris("http://localhost:8083/techgeeknext/login/oauth2/code/techgeeknextclient")
				.autoApprove(false);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		System.err.println("inside endpoints");
		endpoints.authenticationManager(authenticationManager).userDetailsService(authDao).tokenStore(tokenStore())
				.accessTokenConverter(tokenEnhancer());
	}

}
