package com.authorization.admin.oauthConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import com.authorization.admin.service.CustomDetailsService;
@Configuration
//@Order(1)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/*
	 * protected void configure(HttpSecurity http) throws Exception { http
	 * .authorizeRequests() .antMatchers("/login").permitAll()
	 * .antMatchers("/oauth/authorize") .authenticated() .and().formLogin()
	 * .and().requestMatchers() .antMatchers("/login","/oauth/authorize"); }
	 * 
	 * @Autowired private PasswordEncoder passwordEncoder;
	 * 
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth.inMemoryAuthentication()
	 * .withUser("techgeeknextUser").password(passwordEncoder.encode("testPass"))
	 * .roles("USER"); }
	 * 
	 * public void configure(AuthorizationServerSecurityConfigurer oauthServer)
	 * throws Exception { oauthServer.checkTokenAccess("permitAll()"); }
	 * 
	 * @Bean
	 * 
	 * @Override public AuthenticationManager authenticationManagerBean() throws
	 * Exception { return super.authenticationManagerBean(); }
	 */
	
	
	
	  @Autowired
	   private CustomDetailsService customDetailsService;

//	   @Bean
//	   public PasswordEncoder encoder() {
//	      return new BCryptPasswordEncoder();
//	   }
//	   @Override
//	   @Autowired
//	   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//	      auth.userDetailsService(customDetailsService).passwordEncoder(encoder());
//	   }
//	   
		
//		protected void configure(HttpSecurity http) throws Exception {
//			http.authorizeRequests().antMatchers("/generateHash").permitAll().antMatchers("/oauth/authorize").authenticated()
//					.and().formLogin().and().requestMatchers().antMatchers("/generateHash");
//		}
//		 
	   @Override
	    protected void configure(HttpSecurity http) throws Exception {
	       http.authorizeRequests()
	               .anyRequest().authenticated()
	               .and().oauth2Login();
	    }
	  
	   @Override
	   public void configure(WebSecurity web) throws Exception {
	      web.ignoring();
	   }
	   @Override
	   @Bean
	   public AuthenticationManager authenticationManagerBean() throws Exception {
	      return super.authenticationManagerBean();
	   }
}
